package cloudinit

import "github.com/mitchellh/go-homedir"
import "io/ioutil"
import "gopkg.in/yaml.v2"

type Growpart struct {
	Devices                  []string
	Ignore_growroot_disabled bool
	Mode                     string
}

type User struct {
	Name                string
	Lock_passwd         bool
	Plain_text_passwd   string
	Ssh_authorized_keys []string
  Sudo                []string
}

type CloudInit struct {
	Package_update  bool
	Package_upgrade bool
	Ssh_pwauth      bool
	Growpart        Growpart
	Users           []User
}

func GenerateUserData(username string, password string) string {
	homedir, _ := homedir.Dir()
	SSHSublicKey, _ := ioutil.ReadFile(homedir + "/.ssh/id_rsa.pub")
	var object = CloudInit{Package_update: true,
		Package_upgrade: true,
		Ssh_pwauth:      true,
		Growpart: Growpart{Devices: []string{"/"},
			Ignore_growroot_disabled: true,
			Mode:                     "auto"},
		Users: []User{User{Name: username,
			Plain_text_passwd:   password,
			Lock_passwd:         false,
			Ssh_authorized_keys: []string{string(SSHSublicKey)},
			Sudo: []string{string("ALL=(ALL) NOPASSWD:ALL")}}}}
	text, _ := yaml.Marshal(&object)
	return "#cloud-init\n" + string(text)
}

type MetaData struct {
	Hostname       string `yaml:"hostname"`
	SharedHostname string `yaml:"shared-hostname"`
	PublicHostname string `yaml:"public-hostname"`
	InstanceID     int    `yaml:"instance-id"`
}

func GenerateMetaData(hostname string) string {
	var object = MetaData{Hostname: hostname,
		SharedHostname: hostname,
		PublicHostname: hostname,
		InstanceID:     1}
	text, _ := yaml.Marshal(&object)
	return string(text)
}
