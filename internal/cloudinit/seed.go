package cloudinit

import (
	"io"
	"net/http"
	net_url "net/url"
	"os"
)

type WriteCounter struct {
	Total uint64
}

func (wc *WriteCounter) Write(p []byte) (int, error) {
	n := len(p)
	wc.Total += uint64(n)
	return n, nil
}

func SaveIsoAsFile(filepath string, url string, hostname string, username string, password string) error {
	out, err := os.Create(filepath + ".tmp")
	if err != nil {
		return err
	}
	defer out.Close()

	UserData := GenerateUserData(username, password)
	MetaData := GenerateMetaData(hostname)

	resp, err := http.PostForm(url, net_url.Values{
		"user-data": {UserData},
		"meta-data": {MetaData}})

	if err != nil {
		return err
	}
	defer resp.Body.Close()
	counter := &WriteCounter{}
	_, err = io.Copy(out, io.TeeReader(resp.Body, counter))
	if err != nil {
		return err
	}
	err = os.Rename(filepath+".tmp", filepath)
	if err != nil {
		return err
	}
	return nil
}
