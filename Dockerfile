FROM golang

RUN go get -u -v github.com/kardianos/govendor

WORKDIR /go/src/gitlab.com/egeneralov/build-cloud-init
ADD vendor ./vendor/
RUN govendor sync -v

ENV CGO_ENABLED=0 GOOS=linux GOARCH=amd64 
ADD . .
RUN go build -a -tags netgo -ldflags '-w' -v -o /go/bin/build-cloud-init .

FROM scratch
COPY --from=0 /go/bin/build-cloud-init /bin/
ENTRYPOINT ["/bin/build-cloud-init"]
