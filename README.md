
#### install

    brew cask install build-cloud-init.rb

#### debug install

    brew cask install --verbose --debug build-cloud-init.rb

#### uninstall

    brew cask uninstall build-cloud-init
