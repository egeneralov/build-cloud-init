cask 'build-cloud-init' do
  name 'build-cloud-init'
  homepage 'https://github.com/egeneralov/build-cloud-init'

  version '1.0.0'
  sha256 'c3528091f92fbfebb825c7c3273dd3e288640ad778a98ec1e3350c902cf0f669'
  url "https://gitlab.com/egeneralov/build-cloud-init/-/jobs/artifacts/master/download?job=darwin-amd64"

  binary 'build-cloud-init-darwin-amd64', target: 'build-cloud-init'
end
