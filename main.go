package main

import (
	"flag"
	"fmt"
	cloudinit "gitlab.com/egeneralov/build-cloud-init/internal/cloudinit"
)

func main() {

	endpoint := flag.String("endpoint", "https://build-cloud-init-iso.herokuapp.com/", "Path to running egeneralov/build-cloud-init api")
	hostname := flag.String("hostname", "debian.local", "cloud-init hostname")
	path := flag.String("path", "seed.iso", "save to file")
	mode := flag.String("mode", "save", "[save,user,meta]")

	username := flag.String("username", "root", "user name")
	password := flag.String("password", "toor", "user password")

	flag.Parse()

	if *mode == "save" {
		cloudinit.SaveIsoAsFile(*path, *endpoint, *hostname, *username, *password)
	} else if *mode == "user" {
		fmt.Println(cloudinit.GenerateUserData(*username, *password))
	} else if *mode == "meta" {
		fmt.Println(cloudinit.GenerateMetaData(*hostname))
	} else {
		panic("not valid mode: " + *mode)
	}
}
